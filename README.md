OpenTele Android Client
=======================
Android-based patient app for the OpenTele platform.

Please visit our OpenTele wiki page, found on the 4S website, for further information about the OpenTele platform at: 

[http://4s-online.dk/wiki/doku.php?id=opentele:overview](http://4s-online.dk/wiki)